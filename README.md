# Performance Analysis

## data pre-preprocessing

We've read and concatenate the dataset using pandas and the read-csv.py script.

## Exploratory Data Analysis

To better understand the dataset, we've created different correlation matrix. We've use the pearson correlation
coefficient.


### Personalities vs Performance

This matrix shows the correlation between performance and personality.

There is a strong positive correlation between the performance and these personality aspect:
* E1 - I am the life of the party.
* E3 - I feel comfortable around people.
* E5 - I start conversations.
* E7 - I talk to a lot of different people at parties.
* E9 - I don't mind being the center of attention.
* N7 - I change my mood a lot.
* N8 - I have frequent mood swings.
* A9 - I feel others' emotions.
* O10 - I am full of ideas.

There is a strong negative correlation between the performance and these personality aspect:
* E2 - I don't talk a lot.
* A5 - I am not interested in other people's problems.
* A7 - I am not really interested in others.
* O6 - I do not have a good imagination.


![Alt text](/chart/performance-vs-personality.png?raw=true "Optional Title")

### Social demographics vs personalities, performance

The first chart shows the correlation between performance and social demographics features.

There is a positive correlation between the performance and the female gender.


![Alt text](/chart/perf-vs-demo.png?raw=true "Optional Title")

The second chart shows the correlation between performance and countries features.

There is a small positive correlation between the performance and these countries:
* Trinidad and Tobago
* Turkey
* India

![Alt text](/chart/perf-vs-country.png?raw=true "Optional Title")

The third chart shows the correlation between personalities and social demographics features.

There is a positive correlation between the fact that people are not English native speakers and these personalities features:
* A2	I am interested in people.
* A3	I insult people.
* A8	I take time out for others.




![Alt text](/chart/pers-vs-demo.png?raw=true "Optional Title")

The fourth chart shows the correlation between personalities and countries.

![Alt text](/chart/pers-vs-country.png?raw=true "Optional Title")


## prediction

The prediction model is based on XGBoost algorithm and as it is a multiclassification task
 we are using the multi:softmax parameters.
 
We've created this model by performing a grid search for paramaters and number of trees using a cross fold validation
method.

Then we've split the dataset into training and testing and we've build a confusion matrix and an evaluation report
for each category of performance using precision, recall and f-measure.

The application shows a f-1 of 78% in average. For the category 5 of performance the system performs well
with a f-1 of 90%.


### evaluation

             precision    recall  f1-score   support

          1       0.87      0.88      0.87      1364
          2       0.74      0.66      0.69      1279
          3       0.68      0.66      0.67      1282
          4       0.72      0.77      0.74      1260
          5       0.87      0.92      0.90      1323

    avg/total     0.78      0.78      0.78      6508


![Alt text](/chart/confusion-matrix.png?raw=true "Optional Title")

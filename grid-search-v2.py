from xgboost import XGBClassifier, DMatrix, cv
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
from sklearn.model_selection import GridSearchCV

import random
seed=3
random.seed(seed)
np.random.seed(seed)

df = pd.read_csv("data/concat.csv")
features_list = df.columns[1:]

df_country = pd.get_dummies(df['country'])
df_final = pd.concat([df_country,df], axis=1)
df_final = df_final.drop('country', 1)

# split data into features and labels
features = df_final.values[:,1:213]
labels = df_final.values[:,214]

test_size = 0.33
X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=test_size, random_state=seed)

xgdmat = DMatrix(X_train, y_train) # Create our DMatrix to make XGBoost more efficient

model=XGBClassifier(base_score=0.5, booster='gbtree', colsample_bylevel=1,
       colsample_bytree=1, gamma=0, learning_rate=0.1, max_delta_step=1,
       max_depth=8, min_child_weight=1, missing=None, n_estimators=100,
       n_jobs=1, nthread=None, objective='multi:softmax', random_state=0,
       reg_alpha=0, reg_lambda=1, seed=seed,
       silent=True, subsample=1)

# 5 fold cross validation is more accurate than using a single validation set
cv_folds = 5
early_stopping_rounds = 100
xgb_param = model.get_xgb_params()
xgb_param['num_class'] = 6

cvresult = cv(xgb_param, xgdmat, num_boost_round=3000, nfold=cv_folds,
                      early_stopping_rounds=early_stopping_rounds, seed=seed)

print(cvresult)

print("Optimal number of trees (estimators) is %i" % cvresult.shape[0])
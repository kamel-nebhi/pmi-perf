from scipy import stats
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style="darkgrid")


df = pd.read_csv("data/concat.csv")
features_list = df.columns[1:]
df_personality = df.columns[7:35]
#end is 57


df_engnat = pd.get_dummies(df['engnat'],prefix="en",prefix_sep='')
df_gender = pd.get_dummies(df['gender'],prefix="g",prefix_sep='')
df_hand = pd.get_dummies(df['hand'],prefix="h",prefix_sep='')

# create age classification
bins = [0, 35, 45, 55, 60, 120]
group_names = ['-35', '35-45', '45-55', '55-60', '60+']
df['categories'] = pd.cut(df['age'], bins, labels=group_names)
df_age_cat = pd.get_dummies(df['categories'],prefix="",prefix_sep="")


df_final = pd.concat([df_engnat,df_gender,df_hand,df_age_cat,df], axis=1)
df_final = df_final.drop('engnat', 1)
df_final = df_final.drop('gender', 1)
df_final = df_final.drop('hand', 1)
df_final = df_final.drop('age', 1)

#print(df['country'].value_counts())
#print(df_final)
#features = ['engnat_1','engnat_2','gender_1','gender_2','gender_3','hand_1','hand_2','hand_3','age_cat_-35','age_cat_35-45','age_cat_45-55','age_cat_55-60','age_cat_60+','country_US','country_FR']
features = []
features = ['en1','en2','g1','g2','g3','h1','h2','h3','-35','35-45','45-55','55-60','60+']
features.extend(df_personality)

df_test = df_final[features]

pearson = df_test.corr(method='pearson')

corr_with_target = pearson.ix[-1][:-1]
predictivity = corr_with_target[abs(corr_with_target).argsort()[::-1]]
#print(predictivity)

# Generate a mask for the upper triangle
mask = np.zeros_like(pearson, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(25, 25))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(250, 10, n=5, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(pearson, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.4, cbar_kws={"shrink": .5})

plt.savefig('pers-vs-demo.png', bbox_inches='tight', tight_layout=True)


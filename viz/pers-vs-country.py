from scipy import stats
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style="darkgrid")


df = pd.read_csv("data/concat.csv")
features_list = df.columns[1:]
df_personality = df.columns[7:35]

df_country = pd.get_dummies(df['country'],prefix="",prefix_sep='')

df_final = pd.concat([df,df_country], axis=1)
df_final = df_final.drop('country', 1)

df_country = df_final.columns[58:]

#print(df['country'].value_counts())

features = []
features.extend(df_personality)
features.extend(df_country)

df_test = df_final[features]

pearson = df_test.corr(method='pearson')

corr_with_target = pearson.ix[-1][:-1]
predictivity = corr_with_target[abs(corr_with_target).argsort()[::-1]]
#print(predictivity)

# Generate a mask for the upper triangle
mask = np.zeros_like(pearson, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(70, 70))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(250, 10, n=5, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(pearson, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.4, cbar_kws={"shrink": .5})

plt.savefig('data/pers-vs-country.png', bbox_inches='tight', tight_layout=True)


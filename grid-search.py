from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
from sklearn.model_selection import GridSearchCV

import random
seed=3
random.seed(seed)
np.random.seed(seed)

df = pd.read_csv("data/concat.csv")
features_list = df.columns[1:]

df_country = pd.get_dummies(df['country'])
df_final = pd.concat([df_country,df], axis=1)
df_final = df_final.drop('country', 1)

# split data into features and labels
features = df_final.values[:,1:213]
labels = df_final.values[:,214]

test_size = 0.33
X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=test_size, random_state=seed)


cv_params = {'max_depth': [3,5,8], 'min_child_weight': [1,3,7]}
ind_params = {'learning_rate': 0.1, 'n_estimators': 10, 'seed':0, 'subsample': 0.8, 'colsample_bytree': 0.8,
             'objective': 'multi:softmax'}
optimized_GBM = GridSearchCV(XGBClassifier(**ind_params),
                            cv_params,
                             scoring='accuracy',cv=5,n_jobs = -1)

optimized_GBM.fit(X_train, y_train)

# summarize results
print("Best: %f using %s" % (optimized_GBM.best_score_, optimized_GBM.best_params_))
means = optimized_GBM.cv_results_['mean_test_score']
stds = optimized_GBM.cv_results_['std_test_score']
params = optimized_GBM.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))


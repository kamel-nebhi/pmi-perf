from xgboost import XGBClassifier, DMatrix, cv
from sklearn.model_selection import train_test_split
import pandas as pd
from sklearn.metrics import classification_report, confusion_matrix
import matplotlib.pyplot as plt
import itertools
import random
import numpy as np

seed=3
random.seed(seed)
np.random.seed(seed)


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

df = pd.read_csv("data/concat.csv")
features_list = df.columns[1:]

df_country = pd.get_dummies(df['country'])
df_final = pd.concat([df_country,df], axis=1)
df_final = df_final.drop('country', 1)

# split data into features and labels
features = df_final.values[:,1:213]
labels = df_final.values[:,214]

test_size = 0.33
X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=test_size, random_state=seed)

# best model is 191 number of trees
model=XGBClassifier(base_score=0.5, booster='gbtree', colsample_bylevel=1,
       colsample_bytree=1, gamma=0, learning_rate=0.1, max_delta_step=1,
       max_depth=8, min_child_weight=1, missing=None, n_estimators=191,
       n_jobs=1, nthread=None, objective='multi:softmax', random_state=0,
       reg_alpha=0, reg_lambda=1, seed=seed,
       silent=True, subsample=1)

final = model.fit(X_train,y_train)

y_pred = model.predict(X_test)
predictions = [round(value) for value in y_pred]
cm = confusion_matrix(y_test, predictions)

cr = classification_report(y_test, predictions)
print(cr)

plt.figure()
plot_confusion_matrix(cm, classes=['1','2','3','4','5'],
                      title='Confusion matrix, without normalization')

plt.savefig('confusion-matrix.png')
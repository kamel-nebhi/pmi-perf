import pandas as pd

dataFile = "data/data.csv"
perfFile = "data/performance.csv"

df_data = pd.read_csv(dataFile, delimiter="\t", encoding="utf8")
features_list_data = df_data.columns[0:]

df_performance = pd.read_csv(perfFile, delimiter="\t", encoding="utf8")

#print(df_performance['performance'].value_counts())

df_concat = pd.concat([df_data, df_performance], axis=1)
features_list_concat = df_concat.columns[0:]


df_concat.to_csv("data/concat.csv",index=False)

print(features_list_concat)